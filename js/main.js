// Accordion

(function($) {
  var allPanels = $('#category-accordion .subcategory').hide();

  $('#category-accordion > li > a').click(function() {
      $this = $(this);
      $target =  $this.next();

      if($target.hasClass('active')){
        $target.removeClass('active').slideUp();
      }else{
        allPanels.removeClass('active').slideUp();
        $target.addClass('active').slideDown();
      }

    return false;
  });
})(jQuery);

// PDP Thumbnails
(function($) {
  var mainImage = $("#product-image-main");
  var mainImageSrc = ($("#product-image-main").attr("src"));
  console.log(mainImageSrc);

  $('.pdp-image-additional img').click(function(e) {
    e.preventDefault();
    $thumbsImageSrc = $(this).attr("src");
    console.log($thumbsImageSrc);
    mainImage.attr("src",$thumbsImageSrc);
  });

})(jQuery);

// Navigation
(function($) {
  $mobileNavToggle = $('.dl-trigger');

  $mobileNavToggle.click(function(){
    $(this).next().slideToggle();
  });

})(jQuery);

// Ajax post to update cart status
$(document).ready(function(){

   //Product total count
  var productQuantity = $(this).parent().find('.product-quantity').html();

  $('.cart-product-decr').on('click', function (e) {
    e.preventDefault();
    var productId = $(this).parent().parent().find('.productId').html();
    var productQuantity = $(this).parent().find('.product-quantity').html();

    if (productQuantity > 1) {
        productQuantity -= 1;
        $(this).parent().find('.product-quantity').html(productQuantity);
    }

    //Product total count
    var productPrice = $(this).parent().parent().find('.price').html();
    var productTotal = productQuantity * productPrice;
    $(this).parent().parent().find('.total').html(productTotal);

    //Total amount
    $('.cart-info table tbody').each(function(){
      var totalProducts = 0
        $(this).find('tr').each(function(){
          var totalRow = $(this).find('.total').html()
          totalProducts += +totalRow
        })
        $(this).parent().parent().parent().find('.cart-total-amount span').html(+totalProducts);
    })

    $.ajax({
        type: 'POST',
        url: "/basket/productMinus/"+productId+"/"
    })
  });

  $('.cart-product-inc').on('click', function (e) {
    e.preventDefault();
    var productId = $(this).parent().parent().find('.productId').html();

    var productQuantity = $(this).parent().find('.product-quantity').html();
    productQuantity -= -1;
    $(this).parent().find('.product-quantity').html(productQuantity);

    //Product total count
    var productPrice = $(this).parent().parent().find('.price').html();
    var productTotal = productQuantity * productPrice;
    $(this).parent().parent().find('.total').html(productTotal);

     //Total amount
    $('.cart-info table tbody').each(function(){
      var totalProducts = 0
        $(this).find('tr').each(function(){
          var totalRow = $(this).find('.total').html()
          totalProducts += +totalRow
        })
        $(this).parent().parent().parent().find('.cart-total-amount span').html(+totalProducts);
    })


    $.ajax({
        type: 'POST',
        url: "/profile/addBasket/"+productId+"/"
    })
  });

  $('.cart-product-remove').on('click', function (e) {
    e.preventDefault();
    var productId = $(this).parent().parent().find('.productId').html();

    var deleteRow = $(this).parent().parent();
    deleteRow.fadeOut(400);
    deleteRow.remove();

    // Delete from items
    var cartTotalCount = $('#cart-total-count').html();
    cartTotalCount = cartTotalCount - 1;
    $('#cart-total-count').html(cartTotalCount);

    $.ajax({
        type: 'POST',
        url: "/basket/productRemove/"+productId+"/"
    })
    $('.cart-info table tbody').each(function(){
      var totalProducts = 0
      $(this).find('tr').each(function(){
        var totalRow = $(this).find('.total').html()
        totalProducts += +totalRow
      });
      $(this).parent().parent().parent().find('.cart-total-amount span').html(totalProducts);
    });
    var rowCount = $('.cart-info table tbody tr').length;
    $('#cart-total-count').html(rowCount);
  });
});


// Checkout form validation
$(document).ready(function(){
  $('#checkoutForm').validate({
    rules: {
      userFirstname: {
        required: true
      },
      userLastname: {
        required: true
      },
      userEmail: {
        required: true,
        email: true
      }
    },
    messages: {
      userEmail: {
      email: "Please enter correct email.",
      required: "This field is required."
      }
    },
    submitHandler: function() {
      $('.checkout-module').hide();
      $('.checkout-ty').fadeIn('slow');
      $('#checkoutForm').serialize();
      $('#cart-total-count').html('0');
      $.ajax({
        type: 'POST',
        url: "/checkout/buy/",
        data: $('#checkoutForm').serialize()
      })
    }
  });
});

